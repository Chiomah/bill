defmodule Bill.CRM.Contact do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bill.CRM.Account
  alias Bill.CRM.Note

  schema "contacts" do
    field :city, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :firstname, :string
    field :lastname, :string
    field :phone, :string
    field :street, :string
    field :title, :string
    belongs_to  :account, Account
    has_many :notes, Note

    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [:firstname, :lastname, :email, :phone, :title, :department, :street, :city, :country])
    |> validate_required([:firstname, :lastname, :email, :phone, :title, :department, :street, :city, :country])
  end
end
