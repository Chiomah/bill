defmodule Bill.CRM.Account do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bill.CRM.Opportunity
  alias Bill.CRM.Contact
  alias Bill.CRM.Note

  schema "accounts" do
    field :assignee, :string
    field :billing_address, :string
    field :category, :string
    field :city, :string
    field :comments, :string
    field :country, :string
    field :email, :string
    field :name, :string
    field :phone, :string
    field :post_code, :integer
    field :rating, :integer
    field :street1, :string
    field :street2, :string
    field :tags, :string
    field :website, :string
    has_many :opportunities, Opportunity
    has_many :contacts, Contact
    has_many :notes, Note
    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :assignee, :category, :rating, :tags, :comments, :phone, :email, :billing_address, :street1, :street2, :city, :country, :post_code, :website])
    |> validate_required([:name, :assignee, :category, :rating, :tags, :comments, :phone, :email, :billing_address, :street1, :street2, :city, :country, :post_code, :website])
  end
end
