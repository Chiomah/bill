defmodule Bill.CRM.Opportunity do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bill.CRM.Account
  alias Bill.CRM.Note

  schema "opportunities" do
    field :amount, :decimal
    field :assignee, :string
    field :close_date, :date
    field :comment, :string
    field :discount, :decimal
    field :name, :string
    field :probability, :decimal
    field :stage, :string
    belongs_to :account, Account
    has_many :notes, Note

    timestamps()
  end

  @doc false
  def changeset(opportunity, attrs) do
    opportunity
    |> cast(attrs, [:name, :stage, :close_date, :probability, :amount, :discount, :assignee, :comment])
    |> validate_required([:name, :stage, :close_date, :probability, :amount, :discount, :assignee, :comment])
  end
end
