defmodule Bill.CRM.Lead do
  use Ecto.Schema
  import Ecto.Changeset
  alias Bill.CRM.Note

  schema "leads" do
    field :city, :string
    field :company, :string
    field :country, :string
    field :department, :string
    field :email, :string
    field :firstname, :string
    field :lastname, :string
    field :phone, :string
    field :status, :string
    field :street, :string
    field :title, :string
    has_many :notes, Note

    timestamps()
  end

  @doc false
  def changeset(lead, attrs) do
    lead
    |> cast(attrs, [:firstname, :lastname, :email, :phone, :status, :title, :department, :company, :street, :city, :country])
    |> validate_required([:firstname, :lastname, :email, :phone, :status, :title, :department, :company, :street, :city, :country])
  end
end
