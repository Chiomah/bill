defmodule BillWeb.ContactController do
  use BillWeb, :controller

  alias Bill.CRM
  alias Bill.CRM.Contact

  def index(conn, _params) do
    contacts = CRM.list_contacts()
    render(conn, "index.html", contacts: contacts)
  end

  def new(conn, _params) do
    changeset = CRM.change_contact(%Contact{})
    country = CRM.get_countries
    account =  CRM.list_repo_accounts
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    render(conn, "new.html", changeset: changeset, account: account, country: country, title: title)
  end

  def create(conn, %{"contact" => contact_params}) do
    id = Map.get(contact_params, "account_id")
    account= CRM.get_account!(id)
    case CRM.create_account_contact(account, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact created successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        country = CRM.get_countries
        account =  CRM.list_repo_accounts
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        render(conn, "new.html", changeset: changeset,  account: account, country: country, title: title)
    end
  end

  def show(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    changeset = CRM.build_contact_note(contact)
    render(conn, "show.html", contact: contact, changeset: changeset)
  end

  def edit(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    changeset = CRM.change_contact(contact)
    render(conn, "edit.html", contact: contact, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact" => contact_params}) do
    contact = CRM.get_contact!(id)

    case CRM.update_contact(contact, contact_params) do
      {:ok, contact} ->
        conn
        |> put_flash(:info, "Contact updated successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact: contact, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact = CRM.get_contact!(id)
    {:ok, _contact} = CRM.delete_contact(contact)

    conn
    |> put_flash(:info, "Contact deleted successfully.")
    |> redirect(to: Routes.contact_path(conn, :index))
  end


  def addnote(conn, %{"id" => id, "note" => note_params}) do
    #id = Map.get(note_params, "account_id")
    contact= CRM.get_contact!(id)
    case CRM.create_contact_note(contact, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.contact_path(conn, :show, contact))

      {:error, %Ecto.Changeset{} = changeset} ->


        render(conn, "show.html", changeset: changeset, contact: contact)
    end
end
end
