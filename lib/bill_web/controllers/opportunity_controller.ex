defmodule BillWeb.OpportunityController do
  use BillWeb, :controller

  alias Bill.CRM
  alias Bill.CRM.Opportunity

  def index(conn, _params) do
      opportunities = CRM.list_opportunities()
    render(conn, "index.html", opportunities: opportunities )
  end

  def new(conn, _params) do
      changeset = CRM.change_opportunity(%Opportunity{})
     # account = CRM.list_accounts
      account =  CRM.list_repo_accounts

    render(conn, "new.html", changeset: changeset,  account: account)
  end

  def create(conn, %{"opportunity" => opportunity_params}) do
    id = Map.get(opportunity_params, "account_id")
    account= CRM.get_account!(id)
      case CRM.create_account_opportunity(account, opportunity_params) do
      {:ok, opportunity} ->
        conn
        |> put_flash(:info, "Opportunity created successfully.")
        |> redirect(to: Routes.opportunity_path(conn, :show, opportunity))

      {:error, %Ecto.Changeset{} = changeset} ->
        account =  CRM.list_repo_accounts
        render(conn, "new.html", changeset: changeset,  account: account)


    end
  end

  @spec show(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    opportunity = CRM.get_opportunity!(id)
    changeset = CRM.build_opportunity_note(opportunity)
    render(conn, "show.html", opportunity: opportunity, changeset: changeset)
  end






  # def delete(conn, %{"id" => id}) do
  #   opportunity = CRM.get_opportunity!(id)
  #   {:ok, _opportunity} = CRM.delete_opportunity(opportunity)

  #   conn
  #   |> put_flash(:info, "Opportunity deleted successfully.")
  #   |> redirect(to: Routes.opportunity_path(conn, :index))
  # end


    def addnote(conn, %{"id" => id, "note" => note_params}) do
    #id = Map.get(note_params, "account_id")
    opportunity = CRM.get_opportunity!(id)
    case CRM.create_opportunity_note(opportunity, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.opportunity_path(conn, :show, opportunity))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "show.html", changeset: changeset, opportunity: opportunity)
    end
  end
end
