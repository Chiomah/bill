defmodule BillWeb.AccountController do
  use BillWeb, :controller

  alias Bill.CRM
  alias Bill.CRM.Account
  alias Bill.CRM.Note
  def index(conn, _params) do
    accounts = CRM.list_accounts()
    render(conn, "index.html", accounts: accounts)
  end

  def new(conn, _params) do
    changeset = CRM.change_account(%Account{})

    render(conn, "new.html", changeset: changeset)
  end

  @spec create(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def create(conn, %{"account" => account_params}) do
    case CRM.create_account(account_params) do
      {:ok, account} ->
        conn
        |> put_flash(:info, "Account created successfully.")
        |> redirect(to: Routes.account_path(conn, :show, account))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    account = CRM.get_account!(id)
    notes = CRM.list_notes
    changeset = CRM.build_account_note(account)
    render(conn, "show.html", account: account,  changeset: changeset, notes: notes)
  end


  def addnote(conn, %{"id" => id, "note" => note_params}) do
    #id = Map.get(note_params, "account_id")
    account= CRM.get_account!(id)
    case CRM.create_account_note(account, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.account_path(conn, :show, account))

      {:error, %Ecto.Changeset{} = changeset} ->


        render(conn, "show.html", changeset: changeset, account: account)
    end
  end


  # def delete(conn, %{"id" => id}) do
  #   account = CRM.get_account!(id)
  #   {:ok, _account} = CRM.delete_account(account)

  #   conn
  #   |> put_flash(:info, "Account deleted successfully.")
  #   |> redirect(to: Routes.account_path(conn, :index))
  # end
end
