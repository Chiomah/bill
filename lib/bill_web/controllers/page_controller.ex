defmodule BillWeb.PageController do
  use BillWeb, :controller

  def index(conn, _params) do
    conn
    |> redirect(to: Routes.account_path(conn, :index))
  end
end
