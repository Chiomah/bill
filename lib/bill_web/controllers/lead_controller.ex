defmodule BillWeb.LeadController do
  use BillWeb, :controller

  alias Bill.CRM
  alias Bill.CRM.Lead
  alias Bill.CRM.Contact
  alias Bill.CRM.Account
  alias Ecto.Multi
  alias Bill.Repo

  def index(conn, _params) do
    leads = CRM.list_leads()
    render(conn, "index.html", leads: leads)
  end

  def new(conn, _params) do
    changeset = CRM.change_lead(%Lead{})
    country = CRM.get_countries
    title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
    status= ["New"]
     render(conn, "new.html", changeset: changeset, country: country, title: title, status: status)
  end

  def create(conn, %{"lead" => lead_params}) do
    case CRM.create_lead(lead_params) do
      {:ok, lead} ->
        conn
        |> put_flash(:info, "Lead created successfully.")
        |> redirect(to: Routes.lead_path(conn, :show, lead))

      {:error, %Ecto.Changeset{} = changeset} ->
        country = CRM.get_countries
        title = ["Mr", "Mrs", "Miss", "Dr.", "Prof."]
        status= ["New"]
        render(conn, "new.html", changeset: changeset, country: country, title: title, status: status)
    end
  end

  @spec show(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    accounts =  CRM.list_repo_accounts
    lead = CRM.get_lead!(id)
    notes = CRM.list_notes
    changeset = CRM.change_lead(%Lead{})
    render(conn, "show.html", lead: lead, accounts: accounts, changeset: changeset, notes: notes)
  end

  def edit(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    changeset = CRM.change_lead(lead)
    render(conn, "edit.html", lead: lead, changeset: changeset)
  end


    def update(conn, %{"id" => id, "lead" => lead_params}) do
      lead = CRM.get_lead!(id)
      contact_params = convert_lead_to_contact(lead, lead_params["account_id"])
      account_id = Map.get(contact_params, "account_id")
      IO.inspect contact_params
      account = CRM.get_account!(account_id)
      status = %{"status" => "Converted"}

      case create_contact(account, contact_params) do
        {:ok} ->
          case CRM.update_lead(lead, status) do
            {:ok, lead} ->
              conn
              |> put_flash(:info, "Lead successfully linked to account.")
              |> redirect(to: Routes.lead_path(conn, :index))

            {:error, %Ecto.Changeset{} = changeset} ->
              accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
              conn
              |> put_flash(:info, "Error linking lead to account")
              |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
              # render(conn, "show.html", lead: lead, accounts: accounts, changeset: changeset)
          end
        {:error} ->
          accounts = CRM.list_accounts |> Enum.map(&{&1.name, &1.id})
          conn
          |> put_flash(:info, "Error linking lead to account")
          |> render("show.html", lead: lead, accounts: accounts, changeset: %Ecto.Changeset{})
      end


   end

  def delete(conn, %{"id" => id}) do
    lead = CRM.get_lead!(id)
    {:ok, _lead} = CRM.delete_lead(lead)

    conn
    |> put_flash(:info, "Lead deleted successfully.")
    |> redirect(to: Routes.lead_path(conn, :index))
  end


  defp create_contact(account, contact_params) do
    case CRM.create_account_contact(account, contact_params) do
      {:ok, contact} -> {:ok}
      {:error, %Ecto.Changeset{} = changeset} -> {:error}
    end
  end

  defp convert_lead_to_contact(lead, account_id) do

    contact_params = %{"account_id" => account_id, "city" => lead.city, "country" => lead.country,
      "department" => lead.department, "email" => lead.email, "firstname" => lead.firstname,
      "lastname" => lead.lastname, "phone" => lead.phone, "street" => lead.street, "title" => lead.title}
  end


  def addnote(conn, %{"id" => id, "note" => note_params}) do
    #id = Map.get(note_params, "account_id")
    lead = CRM.get_lead!(id)
    case CRM.create_lead_note(lead, note_params) do
      {:ok, note} ->
        conn
        |> put_flash(:info, "Note created successfully.")
        |> redirect(to: Routes.lead_path(conn, :show, lead))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "show.html", changeset: changeset, lead: lead)
    end
  end

end
