defmodule Bill.Repo.Migrations.CreateLeads do
  use Ecto.Migration

  def change do
    create table(:leads) do
      add :firstname, :string
      add :lastname, :string
      add :email, :string
      add :phone, :string
      add :status, :string, default: "New"
      add :title, :string
      add :department, :string
      add :company, :string
      add :street, :string
      add :city, :string
      add :country, :string

      timestamps()
    end

  end
end
