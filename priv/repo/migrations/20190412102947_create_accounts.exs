defmodule Bill.Repo.Migrations.CreateAccounts do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :name, :string
      add :assignee, :string
      add :category, :string
      add :rating, :integer
      add :tags, :string
      add :comments, :string
      add :phone, :string
      add :email, :string
      add :billing_address, :string
      add :street1, :string
      add :street2, :string
      add :city, :string
      add :country, :string
      add :post_code, :integer
      add :website, :string

      timestamps()
    end

  end
end
