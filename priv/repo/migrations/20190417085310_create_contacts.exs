defmodule Bill.Repo.Migrations.CreateContacts do
  use Ecto.Migration

  def change do
    create table(:contacts) do
      add :firstname, :string
      add :lastname, :string
      add :email, :string
      add :phone, :string
      add :title, :string
      add :department, :string
      add :street, :string
      add :city, :string
      add :country, :string
      add :account_id, references(:accounts, on_delete: :nothing)

      timestamps()
    end

    create index(:contacts, [:account_id])
  end
end
