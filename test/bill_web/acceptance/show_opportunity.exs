
  defmodule BillWeb.Acceptance.ShowAccountPageTest do
    use Bill.DataCase
    use Hound.Helpers
    hound_session()




    test "succesfully navigate to show_account page" do

        navigate_to("/opportunities/1")


        detail = find_element(:css, ".a_text") |> visible_text()
        name = find_element(:css, ".name") |> visible_text()
        assignee= find_element(:css,  ".assignee") |> visible_text()
        stage = find_element(:css,,  ".stage") |> visible_text()
        amount = find_element(:css, ".amount") |> visible_text()
        probability = find_element(:css, ".probability") |> visible_text()
        close_date = find_element(:css, ".close_date") |> visible_text()
        discount = find_element(:css, ".discount") |> visible_text()
        button =find_element(:css, ".btn")|> visible_text()
        assert detail  == "Details"
        assert name  == "Account"
        assert assignee == "Assigned to"
        assert stage == "Stage"
        assert probability  == "Probability "
        assert discount == "Discount"
        assert button == "<Back"


    end
  end
