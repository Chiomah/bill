
  defmodule BillWeb.Acceptance.ShowAccountPageTest do
    use Bill.DataCase
    use Hound.Helpers
    hound_session()




    test "succesfully navigate to show_account page" do

        navigate_to("/accounts/1")


        detail = find_element(:css, ".a_text") |> visible_text()
        name = find_within_element(table, :css, ".name") |> visible_text()
        assignee= find_within_element(table, :css, ".assignee") |> visible_text()
        stage = find_within_element(table, :css, ". stage") |> visible_text()
        button=find_element(:css, ".btn")|> visible_text()
        assert detail  == "Details"
        assert name  == "Account"
        assert assignee == "Assigned to"
        assert stage == "Stage"
        assert button == "Back"


    end
  end
