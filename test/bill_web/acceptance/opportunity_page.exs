defmodule BillWeb.Acceptance.OpportunitypageTest do
    use Bill.DataCase
    use Hound.Helpers
    hound_session()




    test "succesfully navigate to opportunity page" do

        navigate_to("/opportunities")

        table = find_element(:css, ".table")
        name = find_within_element(table, :css, ".name") |> visible_text()
        assignee= find_within_element(table, :css, ".assignee") |> visible_text()
        stage = find_within_element(table, :css, ".stage") |> visible_text()
        amount = find_within_element(table, :css, ".amount") |> visible_text()
        probability = find_within_element(table, :css, ".probability") |> visible_text()
        close_date = find_within_element(table, :css, ".close_date") |> visible_text()
        discount = find_within_element(table, :css, ".discount") |> visible_text()

        button = find_element(:css, ".btn")|> visible_text()
        assert name  == "Name"
        assert assignee == "Assigned to"
        assert amount == "Amount"
        assert probability == "Probability"
        assert close_date == "Close Date"
        assert discount == "Discount"
        assert button == "Create Opportunity"


    end


    test "navigate to create opportunities page on button click" do
        navigate_to("/opportunities ")
        button= find_element(:class, "link")|> click
        assert current_path() == "/opportunities/new"
      end
  end
