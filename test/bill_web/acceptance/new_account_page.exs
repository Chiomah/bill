
  defmodule BillWeb.Acceptance.NewAccountPageTest do
    use Bill.DataCase
    use Hound.Helpers
    hound_session()




    test "succesfully navigate to new_account page" do

        navigate_to("/accounts/new")


       form = find_element(:css, ".acc_form")
        name = find_within_element(form, :css, ".name") |> visible_text()
        assignee= find_within_element( form , :css, ".assignee") |> visible_text()
        category = find_within_element(form, :css, ".category") |> visible_text()
        phone = find_within_element( form , :css, ".phone") |> visible_text()
        website  = find_within_element(form, :css, ".website" ) |> visible_text()

        button=find_element(:css, ".btn")|> visible_text()

        assert name  == "Name"
        assert assignee == "Assignee"
        assert category == "Category"
        assert phone == "Phone"
        assert  website == "Website"
        assert button == "< Back"


    end

    test "create a new account with valid data" do
      navigate_to("/accounts/new")

      form = find_element(:tag, "form")
      find_within_element(form, :name, "account[name]") |> fill_field("Stanbic")
      find_within_element(form, :name, "account[assignee]") |> fill_field("Ada John")
      find_within_element(form, :name, "account[category]") |> fill_field("Networking")
      find_within_element(form, :name, "account[rating]") |> fill_field("1")
      find_within_element(form, :name, "account[tags]") |> fill_field("Cisco")
      find_within_element(form, :name, "account[comments]") |> fill_field("done")
      find_within_element(form, :name, "account[phone]") |> fill_field("+23412345678")
      find_within_element(form, :name, "account[website]") |> fill_field("www.stanbic.com")
      find_within_element(form, :name, "account[email]") |> fill_field("aj@email.com")
      find_within_element(form, :name, "account[billing_address]") |> fill_field("5 bimu street")
      find_within_element(form, :name, "account[street1]") |> fill_field("street1")
      find_within_element(form, :name, "account[street2]") |> fill_field("street2")
      find_within_element(form, :name, "account[city]") |> fill_field("city")
      find_within_element(form, :name, "account[country]") |> fill_field("country")
      find_within_element(form, :name, "account[post_code]") |> fill_field("5")

      find_element(:tag, "button") |> click

      #assert current_path == "/accounts"
      message = find_element(:class, "alert") |> visible_text()

      assert message == "Account created successfully."

  end



end
