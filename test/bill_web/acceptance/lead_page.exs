defmodule BillWeb.Acceptance.LeadspageTest do
  use Bill.DataCase
  use Hound.Helpers
  hound_session()




  test "succesfully navigate to lead page" do

      navigate_to("/leads")

      table = find_element(:css, ".table")
      name = find_within_element(table, :css, ".fullname") |> visible_text()
      email = find_within_element(table, :css, ".email") |> visible_text()
      phone = find_within_element(table, :css, ".phone") |> visible_text()
     status = find_within_element(table, :css, ".status") |> visible_text()
     company = find_within_element(table, :css, ".company") |> visible_text()

      button = find_element(:css, ".btn")|> visible_text()
      assert name  == "Full Name"
      assert email  == "Email"
      assert phone == "Phone"
      assert company == "Company"
      assert status == "Status"


  end


  test "navigate to create /leads page on button click" do
      navigate_to("/leads ")
      button= find_element(:class, "acc_btn")|> click
      assert current_path() == "/leads/new"
    end
end
