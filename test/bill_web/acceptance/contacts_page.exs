defmodule BillWeb.Acceptance.ContactspageTest do
  use Bill.DataCase
  use Hound.Helpers
  hound_session()




  test "succesfully navigate to contact page" do

      navigate_to("/contacts")

      table = find_element(:css, ".table")
      name = find_within_element(table, :css, ".fullname") |> visible_text()
      email = find_within_element(table, :css, ".email") |> visible_text()
      phone = find_within_element(table, :css, ".phone") |> visible_text()
      account = find_within_element(table, :css, ".account") |> visible_text()


      button = find_element(:css, ".btn")|> visible_text()
      assert name  == "Full Name"
      assert email  == "Email"
      assert phone == "Phone"
      assert account == "Account(s)"



  end


  test "navigate to create contacts page on button click" do
      navigate_to("/contacts ")
      button= find_element(:class, "acc_btn")|> click
      assert current_path() == "/contacts/new"
    end
end
